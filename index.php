<?php
//all settings
ini_set("display_errors",1);
error_reporting(E_ALL);

//include system's files
define("ROOT",dirname(__FILE__));
require_once(ROOT . "/components/Router.php");

//call route
$router = new Router();
$router->run();