<?php
class Router {

  private $routes;

  public function __construct(){
  $routesPath = ROOT."/config/routes.php";
  $this->routes = include($routesPath);
  }

  /**
   * Returns request string
   * @return string
   */
  private function getURL(){
    if (!empty($_SERVER["REQUEST_URI"])){
      return trim($_SERVER["REQUEST_URI"],"/");
    }
  }


  public function run(){
    // get string request
    $uri = $this->getURL();
    if (empty($uri)){
      //default url
      $uri = "index/index";
    };
    /**
     * return controllers actions array
     */
    //check exist request routes.php
    try{
    $header = explode("/",$uri);
    $controllerUri = $header[0];
    $actionInUri = (isset($header[1])) ? $header[1] : "index" ;
    //unset $controllerName and action on uri
      unset($header[0],$header[1]);
      //params array
    $params = $header;
    $actionNameUri = "action".ucfirst($actionInUri);
    //params to string;
    $paramsUri = ltrim($uri,$controllerUri);
      $uriStatus = false;
    foreach ($this->routes as $uriPattern => $patch){
      //check path to route's reGex
      foreach ($patch as $action){
        if (preg_match("~^$action$~",$paramsUri)){
          $uriStatus = true;
        }
      }

      $controllerName = ucfirst($uriPattern."Controller");
      //check to exist this controller and actions

      if ($uriPattern == $controllerUri) {
        $controllerFile = ROOT . "/Controllers/" . $controllerName . ".php";
        if (file_exists($controllerFile) && $uriStatus) {

          include_once($controllerFile);
          //check params to exist on uri and for valid
          $controllerObject = new $controllerName();
          $result = call_user_func_array([$controllerObject,$actionNameUri],$params);
          if ($result != null) {
            break;
          }
        }
      }
      }
    }catch (Exception $em){
     echo "massage: ". $em->getMessage();
    }
  }
}