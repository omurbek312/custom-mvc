<?php
// array key is controller, and values is actions
return [
  //controller name
  "index" => [
    //action name / and params
    "/index/[0-9]+/[0-9]{1}",
    "/test"
  ],
  "second" => ["/index"]
];